import json
import datetime

from shapely.geometry import Polygon

from stele1.filesystem import Project

import stele1.metadata

import stele1contrib.bonus.photo    as bphoto
import stele1contrib.bonus.approach as bapproach
import stele1contrib.bonus.parking  as bparking
import stele1contrib.bonus.area     as barea
import stele1contrib.bonus.climb    as bclimb

class MigrationManager:
    '''manage moving stele1 entities from one project to another

    it's a naive tool, but with simplicity comes transparency.

    nice features to add: cleaning up dangling references, migrate in two directions, join projects, ...'''
    def __init__(self, source=None, target=None):
        self._source = source
        self._target = target
    def migrate_climb(self, climb_uuid):
        try:
          a = self._source.get_climb_by_uuid(climb_uuid)
          self._target.set_climb(a)
          self._source.remove_climb_by_uuid(climb_uuid)
        except:
          print(f'error migrating climb {str(climb_uuid)}')
    def migrate_area(self, area_uuid):
        a = self._source.get_area_by_uuid(area_uuid)
        self._target.set_area(a)
        self._source.remove_area_by_uuid(area_uuid)
    def migrate_photo(self, photo_uuid):
        a = self._source.get_photo_by_uuid(photo_uuid)
        pat = self._source.get_photo_image_by_uuid(photo_uuid)
        self._target.set_photo(a)
        self._target.set_photo_image_by_uuid(photo_uuid, pat)
        self._source.remove_photo_by_uuid(photo_uuid)
    def migrate_parking(self, parking_uuid):
        a = self._source.get_parking_by_uuid(parking_uuid)
        self._target.set_parking(a)
        self._source.remove_parking_by_uuid(parking_uuid)
    def migrate_approach(self, approach_uuid):
        raise NotImplementedError()
    def migrate_entities_intersecting_boundary(self, boundary):
        for id in self._source.parking_uuids():
            l = bparking.location_point(self._source, id)
            if l and boundary.intersects(l):
                self.migrate_parking(id)
        for id in self._source.area_uuids():
            l = barea.location_polygon(self._source, id)
            if l and boundary.intersects(l):
                self.migrate_area(id)
        for id in self._source.climb_uuids():
            l = None
            try:
              l = bclimb.location_point(self._source, id)
            except:
              print(f'can not load climb {id.to_data()}')
            if l and boundary.intersects(l):
                self.migrate_climb(id)
        for id in self._source.photo_uuids():
            l = bphoto.location_point(self._source, id)
            if l and boundary.intersects(l):
                self.migrate_photo(id)
        for id in self._source.approach_uuids():
            l = bapproach.location_path(self._source, id)
            if l and boundary.intersects(l):
                self.migrate_approach(id)

cli_help = '''
usage: stele1contrib.projectops SUBCOMMAND

do things with a project (or projects)

SUBCOMMANDs:
  extract-perimeter SOURCE_PATH TARGET_PATH GEOJSON_POLYGON
'''

def cmd_extract_perimeter(source, target, perimeter):
    a = Project(source)
    b = Project(target)
    p = polygon_from_json_perimeter(perimeter)
    mm = MigrationManager(a, b)
    if not a.exists():
        raise Exception(f"can't find source store {source}")
    if not b.exists():
        b.create()
        b.set_metadata(source_extraction_metadata(a, alternate_name=source))
    mm.migrate_entities_intersecting_boundary(p)

def polygon_from_json_perimeter(j):
    pts = []
    perim = json.loads(j)
    while isinstance(perim[0][0], list): # get to a list of points
        perim = perim[0]
    for pt in perim:
        pts.append((pt[0], pt[1]))
    return Polygon(pts)

def source_extraction_metadata(source, alternate_name='unnamed project'):
    meta = stele1.metadata.Metadata()
    srcname = source.get_metadata().get_name()
    citation = srcname.to_data() if srcname else alternate_name
    ymd = datetime.date.today().isoformat()
    dsc = stele1.metadata.Description.from_data(f'pulled from "{citation}" on {ymd}')
    nm = stele1.metadata.Name.from_data(f'"{citation}" perimeter extract')
    meta.set_description(dsc)
    meta.set_name(nm)
    return meta

if __name__ == '__main__':
    import sys
    if len(sys.argv) <= 1:
        print(cli_help)
    elif sys.argv[1] == 'extract-perimeter':
        cmd_extract_perimeter(sys.argv[2], sys.argv[3], sys.argv[4])
    else:
        raise Exception('unknown subcommand ' + sys.argv[1])
