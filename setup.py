#!/usr/bin/env python3

import setuptools

setuptools.setup(
    name="stele1contrib-projectops",
    version="1.0.0",
    author="Daniel M",
    author_email="dan.mntg@gmail.com",
    description="split, merge, and migrate stele projects",
    url="https://codeberg.org/stele-climbing/stele1contrib-projectops",
    packages=setuptools.find_packages(),
    python_requires='>=3.6',
)
